# 迅虎网络 虎皮椒composer包

#### 介绍
无需营业执照，个人支付微信支付宝接口,不挂机不提现官方结算

#### 软件架构
软件架构说明


#### 安装教程

通过 Composer 安装
```bash
composer require xunhu/hupijiao dev-master
```

#### 使用说明
首先在业务中引入

```php
<?php
use Xunhu\Hupijiao\Hupijiao;

// 配置通信参数
$config = [
    'app_id' => '',   // 配置商户号
    'app_secret'   => ''   // 配置密钥
    'api_url'=>'https://api.xunhupay.com/payment'
];


虎皮椒支付
    public function pay(){
        // 初始化
        $Hupijiao=new Hupijiao($config);
        $data=$this->data;
        //回调地址，服务器post发送成功支付参数到此地址
        $data['notify_url']='http://xxx.com';
        //异步跳转地址，支付成功后跳转地址
        $data['return_url']='http://xxx.com';
        $response=$Hupijiao->request('wx_native',$data);
        return $response;
    }
 接口通过账号签约类型会自动返回支付宝或者微信支付参数
支付宝h5支付和微信jsapi支付直接跳转返回参数内URL链接。
