<?php

require_once __DIR__ . '/vendor/autoload.php';
use Xunhu\Hupijiao\Hupijiao;

class Demo {

    private $config;
    private $data;

    public function __construct() {
        $this->url='https://'.$_SERVER['HTTP_HOST'];

        $this->config=[
            'app_id'=>'2147483647',
            'app_secret'=>'160130736b1ac0d54ed7abe51e44840b',
            'api_url'=>'https://api.xunhupay.com/payment',
        ];

        //构建测试数据
        $orderID=time();
        $this->data=[
            'version' => '1.1',
            'appid' => $this->config['app_id'],
            'trade_order_id'=> $orderID,  //订单编号
            'payment' => 'wechat',
            'total_fee' => 0.1,
            'title' => 'NO.'.$orderID,
            'time' => time(),
            'notify_url'=> '...',  //异步回调地址
            'return_url'=> '...',  //支付成功跳转地址，可携带参数
            'nonce_str' => md5(time()),
            'plugins'=>'...' //备注字段
        ];
    }

    //微信扫码支付
    public function wxNative(){
        $Hupijiao=new Hupijiao($this->config);
        $data=$this->data;
        $data['payment']='wechat';
        $data['notify_url']=$this->url.'/hupijiao-composer/demo.php';
        $data['return_url']=$this->url.'/hupijiao-composer/demo.php?type=wx_native';
        $response=$Hupijiao->request('wx_native',$data);
        ob_start();
        ?>
        <!doctype html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport"
                  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>微信扫码支付</title>
        </head>
        <style>
            html,body{
                height:100%;
                width:100%;
                overflow:hidden;
                margin:0;
                padding:0;
            }
            body{
                background-color: #444444;
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
            }
            body a{
                padding: 10px 50px;
                background-color: #808080;
                text-decoration: none;
            }
            body a:hover{
                background-color: #F4F4F4;
            }
        </style>
        <body>
        <a href="<?php echo $response['url'];?>">测试支付</a>
        </body>
        </html>
        <?php
        return ob_get_clean();
    }

    //支付成功跳转页面
    public function wxReturn(){
        ob_start();
        ?>
        <!doctype html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport"
                  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>微信扫码支付</title>
        </head>
        <style>
            html,body{
                height:100%;
                width:100%;
                overflow:hidden;
                margin:0;
                padding:0;
            }
            body{
                background-color: #444444;
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
            }
            body span{
                font-weight: bold;
                font-size: 38px;
                color: #fff;
            }
        </style>
        <body>
        <span>支付成功</span>
        </body>
        </html>
        <?php
        return ob_get_clean();
    }

    //异步回调
    public function wxNotify(){
        $Hupijiao=new Hupijiao($this->config);
        $Hupijiao->checkResponse($_POST);
        echo 'success';
    }
}

$Demo=new Demo();
if(isset($_POST['hash'])){
    $Demo->wxNotify();
}elseif (isset($_GET['type'])){
    echo $Demo->wxReturn();
}else{
    echo $Demo->wxNative();
}